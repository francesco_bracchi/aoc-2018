(ns aoc-2018.day_1
  (:require [aoc-2018.utils :as utils]))

(defn run
  []
  (->> "day_1/input.txt"
       (utils/get)
       (map read-string)
       (reduce + 0)))
