(ns aoc-2018.day_2
  (:require [aoc-2018.utils :as utils]))

(defn exact?
  [n id]
  (->> id (frequencies) (vals) (some (partial = n))))

(defn exact
  [a n id]
  (if (exact? n id) (inc a) a))

(def init
  [0 0])

(defn step
  [[c2 c3] s]
  [(exact c2 2 s) (exact c3 3 s)])

(defn final
  [[c2 c3]]
  (* c2 c3))

(defn checksum
  [res]
  (->> res (reduce step init) final))

(defn run
  []
  (->> "day_2/input.txt"
       (utils/get)
       (checksum)))
