(ns aoc-2018.day_3
  (:require [aoc-2018.utils :as utils]
            [clojure.string :as string]))

(def x "#1 @ 565,109: 14x24")

(def re #"#[0-9]+\s+@\s+([0-9]+),([0-9]+)\s*:\s+([0-9]+)x([0-9]+)")

(defn parse-one
  [str]
  (let [[_ & ps] (re-matches re str)]
    (map read-string ps)))

(defn make-empty
  [a b]
  (make-array java.lang.Short/TYPE a b))

(defn mark
  [overlap-map [top left width height]]
  (for [x (range width)
        y (range height)]
    (aset overlap-map (+ top x) (+ left y)
          (inc (aget overlap-map (+ top x) (+ left y)))))
  overlap-map)

(defn count-overlaps
  [overlap-map]
  (->> (for [x (range 1000) y (range 1000)]
         (aget overlap-map x y))
       (filter #(>= % 2))
       (count)))

(defn run
  []
  (->> "day_3/input.txt"
       (utils/get)
       (map parse-one)
       (reduce mark (make-empty 1000 1000))
       (count-overlaps)))
