(ns aoc-2018.utils
  (:require [clojure.java.io :as io]
            [clojure.string :as string]))


(defn get
  [res]
  (->> res
       io/resource
       slurp
       string/split-lines))
